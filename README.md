The central mission of InterCoast Colleges is to provide associates degrees and certificate programs for careers in allied health, business, and skilled trade industries and prepare students to meet employer expectations for entry level employment.

Address: 5253 Business Center Dr, Suite B, Fairfield, CA 94534, USA

Phone: 707-421-9700